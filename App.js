import React, {useEffect,useState} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage';

import Index from './src/screens/Index';

const App = () => {
  const [isloggedin,setLogged] = useState(null)

  const detectLogin= async ()=>{
    const token = await AsyncStorage.getItem('token')
    if(token){
        setLogged(true)
    }else{
        setLogged(false)
    }
  }
  useEffect(()=>{
     detectLogin()
  },[])

  return (
      <Index />
  );
};

export default App;
