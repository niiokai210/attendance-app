import React, { useEffect,useState } from 'react';
import { ActivityIndicator, ImageBackground, StyleSheet, TouchableOpacity, Text, View } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import facilitatorLogin from './facilitator/Login';
import studentLogin from './student/Login';

const LoadingScreen = (props) => {
  const detectLogin= async ()=>{
    //const userType = AsyncStorage.getItem('userType')
    //const jsonValue = await AsyncStorage.getItem('@storage_Key')
    //return jsonValue != null ? JSON.parse(jsonValue) : null;
    
    const token = await AsyncStorage.getItem('token')
        if(token){
          /*
          if(userType = 'student'){
            props.navigation.navigate('StudentLogin')
          }else if(userType = 'facilitator'){
              props.navigation.replace("Home")
          }*/
          props.navigation.navigate('StudentLogin')
        }else{
            props.navigation.replace("Home")
        }
  }
  useEffect(()=>{
   detectLogin()
  },[])

  return (
   <View style={styles.loading}> 
    <ActivityIndicator size="large" color="#003f5c" />
   </View>
  );
};

function Login(props) {
    return (
        <View style={styles.container} >
          <ImageBackground source={require('../../assets/App_BG.jpg')} 
          style={{
            flex: 1,
            resizeMode: "cover",
            alignItems: 'center',
            justifyContent: 'center', }}>
            <Text style={styles.logo}>UG</Text>
            <Text style={styles.logo}>Attendance</Text>
            <Text style={styles.logo}>System</Text>
            <TouchableOpacity style={styles.loginBtn} onPress={() => props.navigation.navigate('FacilitatorLogin')}>
            <Text style={styles.loginText}>LOGIN AS FACILITATOR</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.loginBtn} onPress={() => props.navigation.navigate('StudentLogin')}>
            <Text style={styles.loginText}>LOGIN AS STUDENT</Text>
            </TouchableOpacity>
          </ImageBackground>
        </View>
    )
};

const Stack = createStackNavigator();

class Index extends React.Component {
  render(){
    return (
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerStyle: {
              backgroundColor: '#003f5c',
            },
            headerTintColor: '#fff',
          }}
        >
          <Stack.Screen
            name="Home"
            component={Login}
            options={{ headerShown: false,}}
          />
          <Stack.Screen
            name="StudentLogin"
            component={studentLogin}
            options={{ headerShown: false,}}
          />
          <Stack.Screen
            name="FacilitatorLogin"
            component={facilitatorLogin}
            options={{ headerShown: false,}}
          />
        </Stack.Navigator>
        </NavigationContainer>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#003f5c',
  },
  logo:{
    fontWeight:"bold",
    fontSize:50,
    color:"#000",
    margin:0
  },
  loginText:{
    color:"white",
  },
  inputText:{
    height:50,
    color:"white"
  },
  loginBtn:{
    width:"80%",
    backgroundColor:"#000",
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:40,
    marginBottom:10
  },
  loading:{
  flex:1,
  justifyContent:"center",
  alignItems:"center" 
  }
});

export default Index;