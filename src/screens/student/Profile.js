import React, { Component, useState } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';
import axios from 'axios';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import StudentLogin from './Login';

const LoginNav = createStackNavigator();

const ProfileNav =(props)=>{
  return(
    <LoginNav.Navigator initialRouteName="Profile">
      <LoginNav.Screen name="Profile" component={Profile} options={{headerShown: false}} />
      <LoginNav.Screen name="Logout" component={StudentLogin} options={{headerShown: false}} />
    </LoginNav.Navigator>
  )
}

const Profile = (props) => {
  const [uid,setID] = useState("loading");
  const [name,setName] = useState("loading");
  const [email,setEmail] = useState("loading");
  const [programme,setProgramme] = useState("loading");
  const [level,setLevel] = useState("loading");

  const getDetails = async () => {
    
    try {
      const jwtAuth = await AsyncStorage.getItem('logAuth')
      var logAuth = JSON.parse(jwtAuth);
      var token = logAuth.token;
      var userID = logAuth.userID;
      console.log(token);
      console.log(userID);
  
      axios.post('http://10.0.2.2:3000/student/details', {
        student_id: userID,
      })
      .then((response) => {
        //var studentData = JSON.parse(response);
        //return studentData;
        setID(response.data.sid)
        setEmail(response.data.email)
        setName(response.data.name)
        setProgramme(response.data.programme)
        setLevel(response.data.level)
        console.log(response.data);
      }, (error) => {
        console.log("Error, Something went wrong");
      });
    } catch (error) {
      console.log(error);
    }
  }
  getDetails();

  const logout =(props)=>{
    AsyncStorage.removeItem("token").then(()=>{
      props.navigation.replace("Logout")
    })
  }

  return (
    <View style={styles.container}>
        <View style={styles.header}></View>
        <Image style={styles.avatar} source={{uri: 'https://bootdey.com/img/Content/avatar/avatar6.png'}}/>
        <View style={styles.body}>
          <View style={styles.bodyContent}>
            <Text style={styles.hdr}>{name}</Text>
            <Text style={styles.hdr}>{uid}</Text>
            <Text style={styles.hdr}>Programme:</Text>
            <Text style={styles.info}>{programme}</Text>
            <Text style={styles.hdr}>Level:</Text>
            <Text style={styles.info}>{level}</Text>
            <Text style={styles.hdr}>Email:</Text>
            <Text style={styles.info}>{email}</Text>
            
            <TouchableOpacity style={styles.buttonContainer} onPress={() => logout(props)}>
              <Text>Logout</Text>  
            </TouchableOpacity>
          </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  header:{
    backgroundColor: "#000",
    height:200,
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom:10,
    alignSelf:'center',
    position: 'absolute',
    marginTop:130
  },
  name:{
    fontSize:22,
    color:"#000",
    fontWeight:'600',
  },
  body:{
    marginTop:40,
    borderRadius:15,
  },
  bodyContent: {
    alignItems: 'center',
    flexDirection: 'column',
    padding:30,
  },
  name:{
    fontSize:28,
    color: "#000",
    fontWeight: "600"
  },
  info:{
    fontSize:14,
    color: "#000",
    marginTop:10
  },
  hdr:{
    fontSize:16,
    fontWeight: 'bold',
    color: "#000",
    marginTop:10,
    textAlign: 'center'
  },
  buttonContainer: {
    marginTop:10,
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
    backgroundColor: "#ff7f02",
    color: '#fff',
  },
});

export default ProfileNav;