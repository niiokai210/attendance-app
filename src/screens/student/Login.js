import React, { useState } from 'react';
import { ImageBackground, StyleSheet, TouchableOpacity, Text, View, TextInput, Alert } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';


//import facilitatorLogin from './screens/facilitator/';
import StudentDashboard from './Dashboard';

const LoginStack = createStackNavigator();

function LoginStudent() {
  return (
      <LoginStack.Navigator initialRouteName="Home">
        <LoginStack.Screen name="Home" component={StudentLogin} options={{headerShown: true,title:'Student Login'}} />
        <LoginStack.Screen name="StudentDashboard" component={StudentDashboard} options={{headerShown: false,}}/>
      </LoginStack.Navigator>
  );
}

const StudentLogin = (props) => {
  const [sid, setSid] = useState('');
  const [password, setPassword] = useState('');

  const failedLogin = () =>
    Alert.alert(
      "Login Failed!",
      "Enter correct login details",
      [
        {
          text: "Ok",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        }
      ]
    );

    const loginDetailsRequired = () =>
    Alert.alert(
      "Login Failed!",
      "Enter login details",
      [
        {
          text: "Ok",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        }
      ]
    );
  
  const sendCred = async (props)=>{
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          "sid":sid,
          "password":password
        })
    };
    const response = await fetch('http://10.0.2.2:3000/student/signin', requestOptions);
    const data = await response.json();
    //this.setState({ token: data.token, userType: data.userType  });
    if(data.token){
      var logAuth = { 'token': data.token, 'userType': data.userType, 'userID': data.userID, 'userName': data.userName, };
      try {
        //const jsonValue = JSON.stringify(value)
        await AsyncStorage.setItem('logAuth', JSON.stringify(logAuth))
        props.navigation.replace("StudentDashboard")
        console.log(logAuth)
      } catch (e) {
        return failedLogin();
      }
    }else{
      //console.log('Login Failed')
      //props.navigation.replace("Home")
      return loginDetailsRequired();
    }  

    /*fetch("http://10.0.2.2:3000/student/signin",{
      method:"POST",
      headers: { 'Content-Type': 'application/json' },
      body:JSON.stringify({
        "sid":sid,
        "password":password
      })
    })
    .then(function(response){  
      console.log(response)
     })
    .then(async (data)=>{
      if(!data === null){
        var logAuth = { 'token': data.token, 'userType': data.userType};
        try {
          //const jsonValue = JSON.stringify(value)
          await AsyncStorage.setItem('logAuth', JSON.stringify(logAuth))
          //props.navigation.replace("StudentDashboard")
          console.log(logAuth)
        } catch (e) {
          //console.log("error",e)
            //Alert(e)
            console.log("Login Bad")
        }
      }else{
        console.log("Login Bad")
        //props.navigation.replace("Home")
      }  
    })*/
  }
    return (
        <View style={styles.container} >
          <ImageBackground source={require('../../../assets/App_BG.jpg')} 
            style={{
            flex: 1,
            resizeMode: "cover",
            alignItems: 'center',
            justifyContent: 'center', }}>
            <View style={styles.inputView} >
            <TextInput  
                style={styles.inputText}
                label='student_id'
                mode="outlined"
                value={sid}
                onChangeText={(text)=>setSid(text)}
                placeholder="Enter Student ID No..." 
                placeholderTextColor="#003f5c"/>
            </View>
            <View style={styles.inputView} >
            <TextInput  
                style={styles.inputText}
                label='password'
                mode="outlined"
                value={password}
                onChangeText={(text)=>setPassword(text)}
                secureTextEntry={true}
                placeholder="Enter Password..." 
                placeholderTextColor="#003f5c"/>
            </View>
            <TouchableOpacity style={styles.loginBtn} onPress={() => sendCred(props)}>
            <Text style={styles.loginText}>LOGIN</Text>
            </TouchableOpacity>
          </ImageBackground>
        </View>
    );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#003f5c',
  },
  logo:{
    fontWeight:"bold",
    fontSize:50,
    color:"#fb5b5a",
    marginBottom:40
  },
  inputView:{
    width:"80%",
    backgroundColor:"#fff",
    borderRadius:25,
    height:50,
    marginBottom:20,
    justifyContent:"center",
    padding:20
  },
  inputText:{
    height:50,
    color:"#000"
  },
  loginText:{
    color:"white",
  },
  loginBtn:{
    width:"80%",
    backgroundColor:"#000",
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:40,
    marginBottom:10,
    color:"white"
  },
});

export default LoginStudent;