import React, {useState} from 'react';
import { List, ListItem, View, Text, FlatList, StyleSheet, ScrollView } from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

function ViewAttendance() {
  //const [meeting_name,setMName] = useState("loading");
  //const [created_date,setDate] = useState("loading");
  const [attendanceData,setAttD] = useState("loading");

  const getDetails = async () => {
    try {
      const jwtAuth = await AsyncStorage.getItem('logAuth')
      var logAuth = JSON.parse(jwtAuth);
      var userName = logAuth.userName;
  
      axios.post('http://10.0.2.2:3000/meeting/attendance', {
        student_name: userName,
      })
      .then((response) => {
        //var studentData = JSON.parse(response);
        //return studentData;
        //setMName(response.data.sid)
        setAttD(response.data);
        console.log(response.data);
      }, (error) => {
        console.log("Error, Something went wrong");
      });
    } catch (error) {
      console.log(error);
    }
  }
  getDetails();

    return (
      <ScrollView>
        <View>
          
          <View style={styles.list}>
            <Text style={styles.heading}>Meeting Name</Text>
            <View style={{flexDirection: 'row',}}>
                <Text style={styles.dnt}>Date/Time:</Text>
                <Text style={{}}>23-04-2021 / 2:00pm</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.heading}>Meeting Name</Text>
            <View style={{flexDirection: 'row',}}>
                <Text style={styles.dnt}>Date/Time:</Text>
                <Text style={{}}>23-04-2021 / 2:00pm</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.heading}>Meeting Name</Text>
            <View style={{flexDirection: 'row',}}>
                <Text style={styles.dnt}>Date/Time:</Text>
                <Text style={{}}>23-04-2021 / 2:00pm</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.heading}>Meeting Name</Text>
            <View style={{flexDirection: 'row',}}>
                <Text style={styles.dnt}>Date/Time:</Text>
                <Text style={{}}>23-04-2021 / 2:00pm</Text>
            </View>
          </View>
        </View>
    </ScrollView>
    )

};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ff7f02',
  },
  list:{
    fontWeight:"bold",
    fontSize:20,
    color:"#fb5b5a",
    padding:10,
    borderStyle: 'solid',
    borderBottomColor: '#ff7f02',
    borderBottomWidth: 1,
  },
  heading:{
    fontWeight:"bold",
    fontSize:20,
  },
  dnt:{
    fontWeight:"bold",
    fontSize:14,
  },
  bgImage: {
      //width: 800,
      //height: 50,
      flex:1,
      resizeMode: "cover",
      justifyContent: 'center',
      alignItems: 'center',
    },
});

export default ViewAttendance;