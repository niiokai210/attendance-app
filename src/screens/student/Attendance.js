import React, { useState } from 'react';
import { Alert, ImageBackground, View, Text, Button, TouchableOpacity, TextInput, StyleSheet, FormField  } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Ionicons from 'react-native-vector-icons/Ionicons';
import axios from 'axios';
import Geolocation from 'react-native-geolocation-service';
import AsyncStorage from '@react-native-async-storage/async-storage';

import ViewAttendance from './ViewAttendance';

const HomeScreen = (props) => {
    return (
        <View style={styles.container}>
          
          <ImageBackground 
                style={styles.bgImage}
                source={require('../../../assets/App_BG.jpg')}
            >
              <View style={styles.transBG}>
                <Text style={
                    {
                     fontWeight:"bold",
                     fontSize: 24,
                    }
                }>Welcome!</Text>
                <Text style={
                    {
                     fontWeight:"bold",
                     fontSize: 14,
                    }
                }>This is the student dashboard.</Text>
                <Text style={
                    {
                     fontWeight:"bold",
                     fontSize: 14,
                     marginTop: 40,
                    }
                }>Confirm your presence at lectures or meetings in a few taps!</Text>
              </View>
              <View style={{flex:1, flexDirection:'column', justifyContent:'center', alignItems:'center', width:'80%'}}>
                
                  <TouchableOpacity style={styles.tapArea} onPress={() => props.navigation.navigate('MarkAttendance')}>
                    <View style={styles.tapAction}>
                      <Ionicons name="ios-checkmark-circle-sharp" size={50} color='black' style={{marginRight:20}}/>
                      <Text style={
                          {fontWeight: '700', fontSize: 20, color: 'black', textAlign:'left',}
                      }>MARK ATTENDANCE</Text>
                    </View>
                  </TouchableOpacity>
                
                  <TouchableOpacity style={styles.tapArea} onPress={() => props.navigation.navigate('ViewAttendance')}>
                    <View style={styles.tapAction}>
                      <Ionicons name="ios-document-text" size={50} color='black' style={{marginRight:20}}/>
                      <Text style={
                          {fontWeight: '700', fontSize: 20, color: 'black', textAlign:'left',}
                      }>VIEW ATTENDANCE</Text>
                    </View>
                  </TouchableOpacity>

              </View>
            </ImageBackground>
      </View>
    );
  }

const MarkAttendance = (props) => {
  const [mid, setMid] = useState('');

  const attendanceMarked = () =>
    Alert.alert(
      "Success!",
      "You have been marked as present",
      [
        {
          text: "Ok",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        }
      ]
    );

    const attendanceMarkFailed = () =>
    Alert.alert(
      "Failed!",
      "Attendance not marked",
      [
        {
          text: "Ok",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        }
      ]
    );

  const getLoc = async (props) => {
    var locLatitude = '';
    var locLongitude = '';
  
    if (hasLocationPermission) {
      Geolocation.getCurrentPosition(
          (position) => {
            //console.log(position.coords.longitude);
            //console.log(position.coords.latitude);
            locLongitude = position.coords.longitude;
            locLatitude = position.coords.latitude;
            
            axios.post('http://10.0.2.2:3000/meeting/location', {
              mid: mid
            })
            .then((response) => {
              var locData = response.data;
              //const locData = JSON.stringify(locDataArr);
              //console.log(locData.locLongitude);
              //console.log(locData.locLatitude);

              var meetingLocLong = locData.locLongitude;
              var meetingLocLat = locData.locLatitude;
              
              function areLocNear(checkPointLong, checkPointLat, centerPointLong, centerPointLat) {
                var ky = 40000 / 360;
                var kx = Math.cos(Math.PI * centerPointLat / 180.0) * ky;
                var dx = Math.abs(centerPointLong - checkPointLong) * kx;
                var dy = Math.abs(centerPointLat - checkPointLat) * ky;
                var checkResult = Math.sqrt(dx * dx + dy * dy) <= 0.1;
                return checkResult;
              }
              
              //console.log(locLatitude);
              //console.log(locLongitude);
  
              var detectLocationParameter = areLocNear( locLongitude, locLatitude, meetingLocLong, meetingLocLat);
  
              const markAttendance = async () => {
                try {
                  const jwtAuth = await AsyncStorage.getItem('logAuth')
                  var logAuth = JSON.parse(jwtAuth);
                  //var token = logAuth.token
                  var userName = logAuth.userName
                  
                  if (detectLocationParameter === true){
                    axios.post('http://10.0.2.2:3000/meeting/mark-attendance', {
                      meeting_id: mid,
                      student_name: userName,
                    })
                    .then((response) => {
                      attendanceMarked();
                      console.log('You have been marked Present!');
                    }, (error) => {
                      console.log("Error, Something went wrong");
                    });
                  }else if(detectLocationParameter === false){
                    attendanceMarkFailed();
                    console.log('Attendance not marked. Ensure you are within 100m radius of the meeting location')
                  }
                } catch (error) {
                    console.log(error);
                }
              }
  
              markAttendance();
      
            }, (error) => {
              console.log(error);
            });

            
            /*
            axios.post('http://10.0.2.2:3000/meeting/mark-attendance', {
              _id: mid,
              meeting_name: mname,
              meeting_venue: mvenue,
              facilitator: facilitator,
              coordinates: {'longitude': locLongitude, 'latitude': locLatitude}
            })
            .then((response) => {
              console.log('Meeting Created Successfully!');
            }, (error) => {
              console.log("Error, Something went wrong");
            });*/
          },
          (error) => {
            // See error code charts below.
            console.log(error.code, error.message);
          },
          { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
      );
      //return coordsLong,coordsLat;
  
    };
    //return {'longitude': locLongitude, 'latitude': locLatitude};
    //console.log(locLongitude);
    //console.log(locLongitude);
  }

  return (
      <View style={{padding: 10}}>
          <Text style={{padding: 10, fontSize: 24}}>
              Submit Meeting ID to mark attendance
          </Text>
          <TextInput  
            style={styles.inputText}
            //label='facilitator_id'
            mode="outlined"
            value={mid}
            onChangeText={(text)=>setMid(text)}
            placeholder="Type Meeting ID here..." 
            placeholderTextColor="#222"/>
          <TouchableOpacity style={styles.loginBtn} onPress={() => getLoc(props)}>
            <Text style={styles.loginText}>
              Submit</Text>
          </TouchableOpacity>
      </View>
  );
};

class VAttendance extends React.Component {
  render(){
        return (
                <ViewAttendance />
        );
  }
};

const Stack = createStackNavigator();

const hasLocationPermission = async () => {
  /*if (Platform.OS === 'ios') {
    const hasPermission = await hasPermissionIOS();
    return hasPermission;
  }*/

  if (Platform.OS === 'android' && Platform.Version < 23) {
    return true;
  }

  const hasPermission = await PermissionsAndroid.check(
    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  );

  if (hasPermission) {
    return true;
  }

  const status = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  );

  if (status === PermissionsAndroid.RESULTS.GRANTED) {
    return true;
  }

  if (status === PermissionsAndroid.RESULTS.DENIED) {
    ToastAndroid.show(
      'Location permission denied by user.',
      ToastAndroid.LONG,
    );
  } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
    ToastAndroid.show(
      'Location permission revoked by user.',
      ToastAndroid.LONG,
    );
  }

  return false;
};

class Attendance extends React.Component {
  render(){
    return (
        <Stack.Navigator
          screenOptions={{
            headerStyle: {
              backgroundColor: '#000',
            },
            headerTintColor: '#fff',
          }}
        >
          <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="MarkAttendance"
            component={MarkAttendance}
            options={{ title: 'Mark Attendance' }}
          />
          <Stack.Screen
            name="ViewAttendance"
            component={VAttendance}
            options={{ title: 'Attendance' }}
          />
        </Stack.Navigator>
    );
  }
};

const styles = StyleSheet.create({
  logo:{
    fontWeight:"bold",
    fontSize:50,
    color:"#fed403",
  },
  container: {
    flex: 1,
    backgroundColor: '#003f5c',
  },
  inputText:{
    marginBottom: 10,
    borderBottomWidth:1,
    borderStyle:'solid',
  },
  logo:{
    fontWeight:"bold",
    fontSize:50,
    color:"#fb5b5a",
    marginBottom:40
  },
  bgImage: {
      //width: 800,
      //height: 50,
      flex:1,
      resizeMode: "cover",
      justifyContent: 'space-around',
      alignItems: 'center',
    },
  transBG: {
    flex:1,
    padding: 35, 
    height: '50%', 
    width: '80%', 
    marginTop: 50,
    marginBottom: 20,
    backgroundColor: 'rgba(255,255,255,0.5)', 
    borderRadius: 25
  },
  actionBtn: {
    flex:1,
    padding: 15, 
    marginTop: 20,
    marginBottom: 20,
    //height: 20,
    width: '100%', 
    backgroundColor: 'rgba(255,255,255,0.5)', 
    borderRadius: 25
  },
  tapAction: {
    flex: 1,
    flexDirection:'row',
    height: '30%',
    alignItems:"center",
    justifyContent:"center",
    //padding:20 
  },
  tapArea: {
    flex: 1,
    width: '100%',
    backgroundColor:"#fed403",
    borderRadius:15,
    alignItems:"center",
    justifyContent:"center",
    margin:20,
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 10,
  },
  loginBtn:{
    alignSelf: 'center',
    width:"80%",
    backgroundColor:"#fb5b5a",
    color: 'white',
    borderRadius:15,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:40,
    marginBottom:10
  },
});

export default Attendance;