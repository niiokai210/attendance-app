import React, {useState} from 'react';
import { List, ListItem, View, Text, FlatList, StyleSheet, ScrollView } from 'react-native';

function ViewAttendance() {

    return (
      <ScrollView>
        <View>
          <View style={styles.list}>
            <Text style={styles.heading}>Meeting Name</Text>
            <View style={{flexDirection: 'row',}}>
                <Text style={styles.dnt}>Date/Time:</Text>
                <Text style={{}}>23-04-2021 / 2:00pm</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.heading}>Meeting Name</Text>
            <View style={{flexDirection: 'row',}}>
                <Text style={styles.dnt}>Date/Time:</Text>
                <Text style={{}}>23-04-2021 / 2:00pm</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.heading}>Meeting Name</Text>
            <View style={{flexDirection: 'row',}}>
                <Text style={styles.dnt}>Date/Time:</Text>
                <Text style={{}}>23-04-2021 / 2:00pm</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.heading}>Meeting Name</Text>
            <View style={{flexDirection: 'row',}}>
                <Text style={styles.dnt}>Date/Time:</Text>
                <Text style={{}}>23-04-2021 / 2:00pm</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.heading}>Meeting Name</Text>
            <View style={{flexDirection: 'row',}}>
                <Text style={styles.dnt}>Date/Time:</Text>
                <Text style={{}}>23-04-2021 / 2:00pm</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.heading}>Meeting Name</Text>
            <View style={{flexDirection: 'row',}}>
                <Text style={styles.dnt}>Date/Time:</Text>
                <Text style={{}}>23-04-2021 / 2:00pm</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.heading}>Meeting Name</Text>
            <View style={{flexDirection: 'row',}}>
                <Text style={styles.dnt}>Date/Time:</Text>
                <Text style={{}}>23-04-2021 / 2:00pm</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.heading}>Meeting Name</Text>
            <View style={{flexDirection: 'row',}}>
                <Text style={styles.dnt}>Date/Time:</Text>
                <Text style={{}}>23-04-2021 / 2:00pm</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.heading}>Meeting Name</Text>
            <View style={{flexDirection: 'row',}}>
                <Text style={styles.dnt}>Date/Time:</Text>
                <Text style={{}}>23-04-2021 / 2:00pm</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.heading}>Meeting Name</Text>
            <View style={{flexDirection: 'row',}}>
                <Text style={styles.dnt}>Date/Time:</Text>
                <Text style={{}}>23-04-2021 / 2:00pm</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.heading}>Meeting Name</Text>
            <View style={{flexDirection: 'row',}}>
                <Text style={styles.dnt}>Date/Time:</Text>
                <Text style={{}}>23-04-2021 / 2:00pm</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.heading}>Meeting Name</Text>
            <View style={{flexDirection: 'row',}}>
                <Text style={styles.dnt}>Date/Time:</Text>
                <Text style={{}}>23-04-2021 / 2:00pm</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.heading}>Meeting Name</Text>
            <View style={{flexDirection: 'row',}}>
                <Text style={styles.dnt}>Date/Time:</Text>
                <Text style={{}}>23-04-2021 / 2:00pm</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.heading}>Meeting Name</Text>
            <View style={{flexDirection: 'row',}}>
                <Text style={styles.dnt}>Date/Time:</Text>
                <Text style={{}}>23-04-2021 / 2:00pm</Text>
            </View>
          </View>
        </View>
    </ScrollView>
    )

};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#003f5c',
  },
  list:{
    fontWeight:"bold",
    fontSize:20,
    color:"#fb5b5a",
    padding:10,
    borderStyle: 'dotted',
    borderBottomColor: '#222222',
    borderBottomWidth: 1,
  },
  heading:{
    fontWeight:"bold",
    fontSize:20,
  },
  dnt:{
    fontWeight:"bold",
    fontSize:14,
  },
  bgImage: {
      //width: 800,
      //height: 50,
      flex:1,
      resizeMode: "cover",
      justifyContent: 'center',
      alignItems: 'center',
    },
});

export default ViewAttendance;