import * as React from 'react';
import { ImageBackground, View, Text, TouchableOpacity, Button, ScrollView, Image, TextInput, StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import axios from 'axios';

import Attendance from './Meeting';
import ViewAttendance from './ViewAttendance';
import Profile from './Profile';

function Dashboard(props) {
      return (
        <Attendance/>
      )
  };

function MyTabBar({ state, descriptors, navigation }) {
  return (
    <View style={{ flexDirection: 'row' }}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{ flex: 1 }}
          >
            <Text style={{ color: isFocused ? '#ff7f02' : 'black' }}>
              {label}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const Tab = createBottomTabNavigator();

class App extends React.Component{
  render(){
    return (
        <Tab.Navigator         
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;
  
              if (route.name === 'Home') {
                iconName = focused
                  ? 'ios-home'
                  : 'ios-home-outline';
              } else if (route.name === 'Attendnce') {
                iconName = focused ? 'ios-today' : 'ios-today-outline';
              } else if (route.name === 'Profile') {
                iconName = focused ? 'ios-person' : 'ios-person-outline';
              }
  
              // You can return any component that you like here!
              return <Ionicons name={iconName} size={size} color={color} />;
            },
          })}
          tabBarOptions={{
            activeTintColor: 'black',
            inactiveTintColor: '#ffff00',
            style: {backgroundColor: '#ff7f02', paddingBottom:10, paddingTop:10, height: 60, }
          }}>
          <Tab.Screen name="Home" component={Dashboard} />
          <Tab.Screen name="Profile" component={Profile} />
        </Tab.Navigator>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#003f5c',
    },
    logo:{
      fontWeight:"bold",
      fontSize:50,
      color:"#fb5b5a",
      marginBottom:40
    },
    bgImage: {
        //width: 800,
        //height: 50,
        flex:1,
        resizeMode: "cover",
        justifyContent: 'space-around',
        alignItems: 'center',
      },
    transBG: {
      flex:1,
      padding: 35, 
      height: '50%', 
      width: '80%', 
      marginTop: 50,
      marginBottom: 20,
      backgroundColor: 'rgba(255,255,255,0.5)', 
      borderRadius: 25
    },
    actionBtn: {
      flex:1,
      padding: 15, 
      marginTop: 20,
      marginBottom: 20,
      //height: 20,
      width: '100%', 
      backgroundColor: 'rgba(255,255,255,0.5)', 
      borderRadius: 25
    },
    tapAction: {
      flex: 1,
      flexDirection:'row',
      height: '30%',
      alignItems:"center",
      justifyContent:"center",
      //padding:20 
    },
    tapArea: {
      flex: 1,
      width: '100%',
      backgroundColor:"#fed403",
      borderRadius:15,
      alignItems:"center",
      justifyContent:"center",
      margin:20,
      shadowColor: '#000',
      shadowOffset: { width: 1, height: 1 },
      shadowOpacity: 0.8,
      shadowRadius: 2,  
      elevation: 10,
    },
  });

export default App;