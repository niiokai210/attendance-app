import React, { Component, useState } from 'react';
import { Alert, ToastAndroid, PermissionsAndroid, ImageBackground, View, Text, Button, TouchableOpacity, TextInput, StyleSheet, FormField  } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Ionicons from 'react-native-vector-icons/Ionicons';
import axios from 'axios';
import Geolocation from 'react-native-geolocation-service';
import uuid from 'react-native-uuid';
import AsyncStorage from '@react-native-async-storage/async-storage';

import ViewAttendance from './ViewAttendance';

const HomeScreen = (props) => {
    return (
        <View style={styles.container}>
          
          <ImageBackground 
                style={styles.bgImage}
                source={require('../../../assets/App_BG.jpg')}
            >
              <View style={styles.transBG}>
                <Text style={
                    {
                     fontWeight:"bold",
                     fontSize: 24,
                    }
                }>Welcome!</Text>
                <Text style={
                    {
                     fontWeight:"bold",
                     fontSize: 14,
                    }
                }>This is the Facilitator dashboard.</Text>
                <Text style={
                    {
                     fontWeight:"bold",
                     fontSize: 14,
                     marginTop: 40,
                    }
                }>Create Meetings to allow Students / Attendees to mark attendance at lectures or meetings in a few taps!</Text>
              </View>
              <View style={{flex:1, flexDirection:'column', justifyContent:'center', alignItems:'center', width:'80%'}}>
                
                  <TouchableOpacity style={styles.tapArea} onPress={() => props.navigation.navigate('CreateMeeting')}>
                    <View style={styles.tapAction}>
                      <Ionicons name="add-circle-sharp" size={50} color='black' style={{marginRight:20}}/>
                      <Text style={
                          {fontWeight: '700', fontSize: 20, color: 'black', textAlign:'left',}
                      }>CREATE MEETING</Text>
                    </View>
                  </TouchableOpacity>
                
                  <TouchableOpacity style={styles.tapArea} onPress={() => props.navigation.navigate('ViewAttendance')}>
                    <View style={styles.tapAction}>
                      <Ionicons name="ios-document-text" size={50} color='black' style={{marginRight:20}}/>
                      <Text style={
                          {fontWeight: '700', fontSize: 20, color: 'black', textAlign:'left',}
                      }>VIEW ATTENDANCE</Text>
                    </View>
                  </TouchableOpacity>

              </View>
            </ImageBackground>
      </View>
    );
  }

const CreateMeeting = (props) => {
  const [mname, setName] = useState('');
  const [mvenue, setVenue] = useState('');

  const getLoc = (props) => {
    var locLatitude = '';
    var locLongitude = '';
  
    if (hasLocationPermission) {
      Geolocation.getCurrentPosition(
          (position) => {
            //console.log(position.coords.longitude);
            //console.log(position.coords.latitude);
            locLongitude = position.coords.longitude;
            locLatitude = position.coords.latitude;

            var mid = makeid();
            var facilitator = '';

            function makeid() {
              var text = "";
              var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            
              for (var i = 0; i < 9; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            
              return text;
            }
            
            //console.log(locLatitude);
            //console.log(locLongitude);

            const createMeeting = async () => {
              try {
                const jwtAuth = await AsyncStorage.getItem('logAuth')
                var logAuth = JSON.parse(jwtAuth);
                //var token = logAuth.token
                var userName = logAuth.userName
                
                axios.post('http://10.0.2.2:3000/meeting/create', {
                  mid: mid,
                  meeting_name: mname,
                  meeting_venue: mvenue,
                  facilitator: userName,
                  locLongitude: locLongitude,
                  locLatitude: locLatitude
                })
                .then((response) => {
                  meetingCreated();
                  console.log('Meeting Created Successfully!');
                }, (error) => {
                  meetingCreateFail()
                  console.log("Error, Something went wrong");
                });
              } catch (error) {
                  console.log(error);
              }
            }

            createMeeting();
          },
          (error) => {
            // See error code charts below.
            console.log(error.code, error.message);
          },
          { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
      );
      //return coordsLong,coordsLat;
  
    };
    //return {'longitude': locLongitude, 'latitude': locLatitude};
    //console.log(locLongitude);
    //console.log(locLongitude);
  }

  const meetingCreated = () =>
    Alert.alert(
      "Success!",
      "Meeting Created",
      [
        {
          text: "Ok",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        }
      ]
    );

    const meetingCreateFail = () =>
    Alert.alert(
      "Error!",
      "Failed to create meeting",
      [
        {
          text: "Ok",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        }
      ]
    );

  //const getLocCoords = getLoc();

  

    return (
        <View style={{padding: 10}}>
            <Text style={{padding: 10, fontSize: 24}}>
                Enter Meeting Details
            </Text>
            <TextInput  
            style={styles.inputText}
            //label='facilitator_id'
            mode="outlined"
            value={mname}
            onChangeText={(text)=>setName(text)}
            placeholder="Name" 
            placeholderTextColor="#222"/>
            <TextInput  
            style={styles.inputText}
            //label='facilitator_id'
            mode="outlined"
            value={mvenue}
            onChangeText={(text)=>setVenue(text)}
            placeholder="Venue"
            placeholderTextColor="#222"/>
            <TouchableOpacity style={styles.loginBtn} onPress={() => getLoc(props)}>
              <Text style={styles.loginText}>
                Add meeting</Text>
            </TouchableOpacity>
        </View>
    );

};

class VAttendance extends React.Component {
  render(){
        return (
                <ViewAttendance />
        );
  }
};

const Stack = createStackNavigator();

const hasLocationPermission = async () => {
  /*if (Platform.OS === 'ios') {
    const hasPermission = await hasPermissionIOS();
    return hasPermission;
  }*/

  if (Platform.OS === 'android' && Platform.Version < 23) {
    return true;
  }

  const hasPermission = await PermissionsAndroid.check(
    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  );

  if (hasPermission) {
    return true;
  }

  const status = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  );

  if (status === PermissionsAndroid.RESULTS.GRANTED) {
    return true;
  }

  if (status === PermissionsAndroid.RESULTS.DENIED) {
    ToastAndroid.show(
      'Location permission denied by user.',
      ToastAndroid.LONG,
    );
  } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
    ToastAndroid.show(
      'Location permission revoked by user.',
      ToastAndroid.LONG,
    );
  }

  return false;
};

class Meeting extends React.Component {
  render(){
    return (
        <Stack.Navigator
          screenOptions={{
            headerStyle: {
              backgroundColor: '#000',
            },
            headerTintColor: '#fff',
          }}
        >
          <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="CreateMeeting"
            component={CreateMeeting}
            options={{ title: 'Create Meeting' }}
          />
          <Stack.Screen
            name="ViewAttendance"
            component={VAttendance}
            options={{ title: 'Attendance Report' }}
          />
        </Stack.Navigator>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#003f5c',
  },
  inputText:{
    marginBottom: 10,
    borderBottomWidth:1,
    borderStyle:'solid',
  },
  bgImage: {
      //width: 800,
      //height: 50,
      flex:1,
      resizeMode: "cover",
      justifyContent: 'space-around',
      alignItems: 'center',
    },
  transBG: {
    flex:1,
    padding: 35, 
    height: '50%', 
    width: '80%', 
    marginTop: 50,
    marginBottom: 20,
    backgroundColor: 'rgba(255,255,255,0.5)', 
    borderRadius: 25
  },
  actionBtn: {
    flex:1,
    padding: 15, 
    marginTop: 20,
    marginBottom: 20,
    //height: 20,
    width: '100%', 
    backgroundColor: 'rgba(255,255,255,0.5)', 
    borderRadius: 25
  },
  tapAction: {
    flex: 1,
    flexDirection:'row',
    height: '30%',
    alignItems:"center",
    justifyContent:"center",
    //padding:20 
  },
  tapArea: {
    flex: 1,
    width: '100%',
    backgroundColor:"#fed403",
    borderRadius:15,
    alignItems:"center",
    justifyContent:"center",
    margin:20,
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 10,
  },
  loginBtn:{
    alignSelf: 'center',
    width:"80%",
    backgroundColor:"#fb5b5a",
    color: 'white',
    borderRadius:15,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:40,
    marginBottom:10
  },
});

export default Meeting;